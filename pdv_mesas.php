<?php
    $consulta_pdv_aberto = mysql_query("SELECT status FROM caixa01 where id = (select max(id) from caixa01)");
    if (mysql_result($consulta_pdv_aberto,0) == 'Fechado')
    {
        echo '<br><br>
            <div class="ui center aligned grid">
            <div class="ui negative message">
                <i class="close icon"></i>
                <div class="header">
                  Caixa está fechado!
                </div>
                <p>Por favor, faça abertura do caixa para utilizar as Mesas!
              </p>
              <p><a href="pdv.php" class="ui green button">Abrir caixa</a></p></div></div><br><br>';
    }
 else {

     // Criar tabela de mesas condicional 
     $criar_tabela = mysql_query("CREATE TABLE IF NOT EXISTS `pedido_mesa".$id_mesa."` (
                    `id` int(11) NOT NULL AUTO_INCREMENT,
                    `id_produto` int(11) NOT NULL,
                    `quantidade` int(11) NOT NULL,
                    `obs` varchar(80) NOT NULL,
                    `id_garcom` int(11) DEFAULT NULL,
                    PRIMARY KEY (`id`)
                  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
     
     $criar_tabela_aux = mysql_query("CREATE TABLE IF NOT EXISTS `pedido_aux_mesa".$id_mesa."` (
                    `id` int(11) NOT NULL AUTO_INCREMENT,
                    `id_produto` int(11) NOT NULL,
                    `quantidade` int(11) NOT NULL,
                    `obs` varchar(80) NOT NULL,
                    `id_garcom` int(11) DEFAULT NULL,
                    PRIMARY KEY (`id`)
                  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
     
?>
<!--
################ Estilo da DIV Subtotal ##################
-->
<style>
  .subtotal {
    background-color: #eee;
    padding: 40px 5px 40px 5px;
    text-align: right;
    font-size: 28pt;
    border-radius: 3px;
    box-shadow: 2px 2px 6px #ccc;
  }

  .subtotal span {
    text-align: center;
    font-size: 12pt;
    vertical-align: middle;
    margin-right: 2%;

  }
  
#id_of_button {
    
}

.sumir {
    display: none;
  }
</style>
<!--
################ ATALHOS DO TECLADO ##################
-->
<script>

  document.onkeyup=function(e){

   if(e.which == 107){
          window.location.hash = "#venda";
     return false;
   }
  }

</script>

<!--
################ LISTAGEM DE PRODUTOS E PEDIDOS ##################
-->

<?php

    echo '<div class="ui grey pointing inverted menu">';
    $buscaCategorias = mysql_query("SELECT * FROM categorias");
    while ($categoria = mysql_fetch_array($buscaCategorias))
    {
    echo '<a href="#'.$categoria['categoria'].'" class="right floated item '.$categoria['categoria'].'" onclick="location.reload()">'
            . $categoria['categoria']
        . '</a>';
    }
    echo '</div>';

?>
        
<div class="ui two column doubling stackable grid container">
  <div class="column">
    <p>        
        <h3 class='ui center aligned header'>PDV</h3>
    </p>
    <?php
        //include 'teste/autocomplete/mesas/index.php';
      echo "<p>"
            ."<table class='ui small compact table'>"
              ."<thead>"                
                ."<th class='center aligned'>Cod</th>"
                ."<th>Produto</th>"
                ."<th>Preço</th>"
                ."<th class='center aligned'>Qtd</th>"
                ."<th>Total</th>"
                ."<th class='right aligned'>Ação</th>"
              ."</thead>"
      ;
      $query_pdv = mysql_query('
        SELECT
         a.id,
         b.code,
         b.name as Produto,
         a.quantidade,
         b.cost as Preço,
         a.quantidade*b.cost as Total,
         a.obs
         ,b.category_id
       FROM
	        pedido_mesa'.$id_mesa.' a
        INNER JOIN
	         tec_products b
         ON
            a.id_produto = b.id
          ORDER BY a.id');
      while ($pedido=mysql_fetch_array($query_pdv)) {
        echo "<tr>"
            ."<td class='collapsing center aligned'>".$pedido['code']."</td>"    
            ."<td class=''>".$pedido['Produto']."</td>"
              ."<td class='collapsing'>R$ ".$pedido['Preço']."</td>"
              . "<td class='center aligned'><form action='processa_mesa.php' method='post'><input type='hidden' name='mesa' value='".$id_mesa."'><input type='hidden' name='seu_nome2' value='".$pedido['id']."'><input type='text' name='seu_nome' placeholder='".$pedido['quantidade']."' size='2'>x</td>"
              ."<td class='collapsing'>R$ ".$pedido['Total']."</td>"
                ."<td class='right aligned'>"."<a href='balcaoDAO_mesas.php?mesa=".$id_mesa."&id_del=".$pedido['id']."'><i class='trash icon'></i></a>"
                . "<input type='submit' value='Alterar'>"
                . "</form>"
                ."</td>"
            ."</tr>"
        ;
        $subtotal+=$pedido['Total'];
      }
      echo '</table>'
            . '<table class="ui table">';      
      echo "<tr>"
            ."<td colspan='1'><a href='balcaoDAO_mesas.php?mesa=$id_mesa&truncar=yes' class='ui red fluid button'>Cancelar</a></td>"
            //."<td colspan='1'><a href='insere_aux_mesas.php?mesa=$id_mesa' id='botao' class='ui green fluid button'>Finalizar</a></td>"
              ."<td colspan='1'><a href='imprimir_conta.php?mesa=$id_mesa' class='ui basic fluid button'>Fechar</a></td>"
            ."<td colspan='3' rowspan='2' width='80%'><div class='subtotal'><span>subtotal </span>R$ ".number_format($subtotal, 2,',','.')."</div></td>"
          ."</tr>"
            ."<tr>"
            ."<td colspan='2'><a href='#venda' id='botao' class='ui green fluid button'>Finalizar</a></td>"
          ."</tr>"
          ."</table>"
          ."</p>";
      
          
     ?>
  </div>
  <div class="column">
    <p><h3 class='ui center aligned header'>Produtos</h3><br></p>
    <p>
        <?php echo '<a href="search_mesas/modal_sabores.php?mesa='.$id_mesa.'#pizzas" class="ui basic button">2 Sabores</a>';
        echo '<a href="search_mesas/modal_3sabores.php?mesa='.$id_mesa.'#pizzas" class="ui basic button">3 Sabores</a>'; ?>
        <div class="ui bottom attached segment">
      <p>
        <?php
             include 'lista_produtos_mesas.php';
        ?>
          </p>
        </div>
      </p>
    </div>
  </div>

  <script>
  var target = window.location.hash;
  if (target === "#lanches") 
  {
    $('.lanches').addClass('active');
    $('.bebidas').removeClass('active');
    $('.geral').removeClass('sumir');
    //$('#pdv').addClass('active');
    $('.prod2').removeClass('sumir');
  } 
  else if (target === "#bebidas") 
  {
    $('.lanches').removeClass('active');
    $('.bebidas').addClass('active');
    $('.geral').removeClass('sumir');
    //$('#pdv').addClass('active');
    $('.prod1').removeClass('sumir');
  }
  else if (target === "#Pizzas" || target === "")
  {
    $('.lanches').removeClass('active');
    $('.Pizzas').addClass('active');
    $('.geral').removeClass('sumir');
    //$('#pdv').addClass('active');
    $('.prod1').removeClass('sumir');  
  }
  else if (target === "#Esfihas")
  {
    $('.lanches').removeClass('active');
    $('.Esfihas').addClass('active');
    $('.geral').removeClass('sumir');
    //$('#pdv').addClass('active');
    $('.prod2').removeClass('sumir');  
  }
  else if (target === "#Salgados")
  {
    $('.lanches').removeClass('active');
    $('.Salgados').addClass('active');
    $('.geral').removeClass('sumir');
    //$('#pdv').addClass('active');
    $('.prod3').removeClass('sumir');  
  }
  else if (target === "#Beirutes")
  {
    $('.lanches').removeClass('active');
    $('.Beirutes').addClass('active');
    $('.geral').removeClass('sumir');
    //$('#pdv').addClass('active');
    $('.prod4').removeClass('sumir');  
  }
  else if (target === "#Porções")
  {
    $('.lanches').removeClass('active');
    $('.Porções').addClass('active');
    $('.geral').removeClass('sumir');
    //$('#pdv').addClass('active');
    $('.prod5').removeClass('sumir');  
  }
  else if (target === "#Bebidas")
  {
    $('.lanches').removeClass('active');
    $('.Bebidas').addClass('active');
    $('.geral').removeClass('sumir');
    //$('#pdv').addClass('active');
    $('.prod6').removeClass('sumir');  
  }
  else if (target === "#Pastéis")
  {
    $('.lanches').removeClass('active');
    $('.Pastéis').addClass('active');
    $('.geral').removeClass('sumir');
    //$('#pdv').addClass('active');
    $('.prod7').removeClass('sumir');  
  }
  else if (target === "#Lanches")
  {
    $('.lanches').removeClass('active');
    $('.Lanches').addClass('active');
    $('.geral').removeClass('sumir');
    //$('#pdv').addClass('active');
    $('.prod8').removeClass('sumir');  
  }
  else if (target === "#Doces")
  {
    $('.lanches').removeClass('active');
    $('.Doces').addClass('active');
    $('.geral').removeClass('sumir');
    //$('#pdv').addClass('active');
    $('.prod9').removeClass('sumir');  
  }
  else if (target === "#Sorvetes")
  {
    $('.lanches').removeClass('active');
    $('.Sorvetes').addClass('active');
    $('.geral').removeClass('sumir');
    //$('#pdv').addClass('active');
    $('.prod10').removeClass('sumir');  
  }
  else if (target === "#Balas")
  {
    $('.lanches').removeClass('active');
    $('.Balas').addClass('active');
    $('.geral').removeClass('sumir');
    //$('#pdv').addClass('active');
    $('.prod11').removeClass('sumir');  
  }
</script>

<?php
  include 'popup_caixa.php';
  include 'popup_venda_mesas.php';
  include 'popup_motoboy.php';
  include 'rodape.php';
   echo '<div class="sumir">';
    include 'teste/autocomplete/mesas/index.php';
   echo '</div>';
 
?>

</div>
</div>
<?php
}
?>